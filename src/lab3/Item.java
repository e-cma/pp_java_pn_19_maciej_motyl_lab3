package lab3;
// ***************************************************************
//   Item.java
//
//   Reprezentuje rzecz w koszyku.
// ***************************************************************

import java.io.Serializable;
import java.text.NumberFormat;

public class Item implements Serializable
{
    private String nazwa;
    private double cena;
    private int ilosc;


    // -------------------------------------------------------
    //  Tworzy nowa rzecz z zadanych parametrow.
    // -------------------------------------------------------
    public Item (String nazwaRzeczy, double cenaRzeczy, int iloscZakupionych)
    {
	nazwa = nazwaRzeczy;
	cena = cenaRzeczy;
	ilosc = iloscZakupionych;
    }


    // -------------------------------------------------------
    //   Zwraca lancuch znakowy z informacjami o przedmiocie
    // -------------------------------------------------------
    public String toString ()
    {
	NumberFormat fmt = NumberFormat.getCurrencyInstance();

	return (nazwa + " | " + fmt.format(cena) + " | " + ilosc + " szt | "
		+ fmt.format(cena*ilosc));
    }

    // -------------------------------------------------
    //   Zwraca cene jednostkowa przedmiotu
    // -------------------------------------------------
    public double getCena()
    {
	return cena;
    }
    
    

    // -------------------------------------------------
    //   Zwraca nazwe przedmiotu
    // -------------------------------------------------
    public String getNazwa()
    {
	return nazwa;
    }

    // -------------------------------------------------
    //   Zwraca ilosc rzeczy
    // -------------------------------------------------
    public int getIlosc()
    {
	return ilosc;
    }
}  
