package lab3;

import java.awt.EventQueue;
import java.awt.List;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.AbstractListModel;
import javax.swing.JTextArea;

import java.beans.XMLEncoder;
import java.beans.XMLDecoder;
import java.io.*;

public class Sklep {

	private JFrame frame;
	private JTextField ProduktNazwa;
	private JTextField ProduktCena;
	private JTextField ProduktIlosc;
	private	ArrayList<Item> KoszykProdoktow = new ArrayList<Item>();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Sklep window = new Sklep();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Sklep() {
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		
		ProduktNazwa = new JTextField();
		ProduktNazwa.setBounds(338, 11, 86, 20);
		frame.getContentPane().add(ProduktNazwa);
		ProduktNazwa.setColumns(10);
		
		ProduktCena = new JTextField();
		ProduktCena.setBounds(338, 73, 86, 20);
		frame.getContentPane().add(ProduktCena);
		ProduktCena.setColumns(10);
		
		ProduktIlosc = new JTextField();
		ProduktIlosc.setBounds(338, 42, 86, 20);
		frame.getContentPane().add(ProduktIlosc);
		ProduktIlosc.setColumns(10);
		
		JLabel lblNazwa = new JLabel("Nazwa");
		lblNazwa.setBounds(282, 14, 46, 14);
		frame.getContentPane().add(lblNazwa);
		
		JLabel lblIlosc = new JLabel("Ilosc");
		lblIlosc.setBounds(282, 45, 46, 14);
		frame.getContentPane().add(lblIlosc);
		
		JLabel lblCenaJ = new JLabel("Cena J.");
		lblCenaJ.setBounds(282, 76, 46, 14);
		frame.getContentPane().add(lblCenaJ);
		
		JTextArea Koszyk = new JTextArea();
		Koszyk.setEditable(false);
		Koszyk.setBounds(10, 14, 219, 237);
		frame.getContentPane().add(Koszyk);
		
		JButton ProduktDodaj = new JButton("Dodaj");
		ProduktDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				KoszykProdoktow.add(new Item(ProduktNazwa.getText(),Double.parseDouble(ProduktCena.getText()),Integer.parseInt(ProduktIlosc.getText())));
				String Itemki="";
				for(Item x:KoszykProdoktow)
				{
					Itemki=Itemki+"\n"+x.toString();
				}
				Koszyk.setText(Itemki);
				ProduktNazwa.setText("");
				ProduktCena.setText("");
				ProduktIlosc.setText("");
				
			}
		});
		ProduktDodaj.setBounds(338, 104, 89, 23);
		frame.getContentPane().add(ProduktDodaj);
		

		
		JButton Wczytaj = new JButton("Wczytaj");
		Wczytaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			    ObjectInputStream in;
				try {
					in = new ObjectInputStream(new BufferedInputStream(new FileInputStream("Koszyk.ser")));
					KoszykProdoktow = (ArrayList<Item>) in.readObject();
				} catch (FileNotFoundException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
				String Itemki="";
				for(Item x:KoszykProdoktow)
				{
					Itemki=Itemki+"\n"+x.toString();
				}
				Koszyk.setText(Itemki);
		            
				
			}
		});
		Wczytaj.setBounds(335, 228, 89, 23);
		frame.getContentPane().add(Wczytaj);
		
		JButton Zapisz = new JButton("Zapisz");
		Zapisz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			    ObjectOutputStream out;
				try {
					out = new ObjectOutputStream(
					   new BufferedOutputStream(new FileOutputStream("Koszyk.ser")));
					try {
			          out.writeObject(KoszykProdoktow);
			        } finally {
			          out.close();
			        }
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}
		});
		Zapisz.setBounds(239, 228, 89, 23);
		frame.getContentPane().add(Zapisz);
		

		

		

	}
}
